<?php 
namespace Lantera\ReindexFix\Rewrite\Indexer\Category\Product\Action;

class Full  extends \Magento\Catalog\Model\Indexer\Category\Product\Action\Full{ 
	
	 
	 /**
     * Return selects cut by min and max
     *
     * @param \Magento\Framework\DB\Select $select
     * @param string $field
     * @param int $range
     * @return \Magento\Framework\DB\Select[]
     */
    protected function prepareSelectsByRange(\Magento\Framework\DB\Select $select, $field, $range = 100000000)
    {
        return $this->isRangingNeeded() ? $this->connection->selectsByRange(
            $field,
            $select,
            $range
        ) : [
            $select
        ];
    }  
}
   